# Website Makefile for ease of use.

# ====================================================================================
# Project specific variables
# ====================================================================================

CV_LOC = ../CV/cv.pdf
CV_COPY_LOC = org/files/corbin_cv.pdf

# ====================================================================================
# Variables
# ====================================================================================

FUNCS := org-publish-all
SITE_SOURCE := "./build.el"

DATE := $(shell date +"%d %b %y" --date='-0 year')
DATE_LONG := $(shell date +"%d %B %y" --date='-0 year')
DATETIME := $(shell date +"%d %b %y %H:%M:%S" --date='-0 year')
UPDATE_FILE := "./org/updates.org"
TODO_FILE   := "./org/todo.org"
HOME_FILE   := "./org/index.org"
RESIZE_IMG_DIR := _draft/resize_dir

# TOUCH_FILES are those which need to be 'touched' in order for Org export to
# recompile them. This is necessary for most post updates.
FILES_TO_TOUCH="./org/index.org"

# ====================================================================================
# Convenience functions
# ====================================================================================


define touchfiles
	for i in $(FILES_TO_TOUCH); do \
		touch $$i; \
	done
endef

define make_subdir
	if [ ! -d small ]; then \
	  mkdir small; \
	  echo "subdirectory created."; \
	fi
endef

# ====================================================================================
#  make commands
# ====================================================================================


update:
	echo "- *($(DATE))* " | cat - $(UPDATE_FILE) > temp && mv temp $(UPDATE_FILE)
	emacs -nw -Q $(UPDATE_FILE) --eval "(end-of-line)"
	$(call touchfiles)
	sed "s,updated on.*\<,updated on $(DATE_LONG)\<\/,g" $(HOME_FILE) > temp.org && mv temp.org $(HOME_FILE)

todo:
	echo "- [ ] *($(DATETIME))* " >> $(TODO_FILE)
	emacs -nw -Q $(TODO_FILE) --eval "(progn (end-of-buffer) (previous-line) (end-of-line))"

build:
	$(call touchfiles)
	emacs --batch --no-init --load $(SITE_SOURCE) --funcall $(FUNCS)
	@echo "Site built."

deploy: build
	git-chk

resize:
	cd ${RESIZE_IMG_DIR}; $(call make_subdir); for file in *.jpg; do \
	  convert $${file} -resize '500x>' "small/$${file%.*}_small.jpg"; \
	done

serve:
	xdg-open http://0.0.0.0:8000/
	python -m http.server --directory=.

# ==================================================================================
# Project specific commands
# ==================================================================================
cv:
	@if [ -f $(CV_LOC) ]; then \
		cp $(CV_LOC) $(CV_COPY_LOC); \
		echo "CV copied to $(CV_COPY_LOC)."; \
	else echo "No cv found in $(CV_LOC). Aborting."; \
	fi
