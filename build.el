(require 'ox-publish)

(setq org-publish-project-alist
      '(
        ;; Add all site components here.
        ("Ryans_Site" :components ("homepage"
				   "website-static"))
        ("homepage"
         :base-directory "./org"
         :base-extension "org"
         :publishing-directory "./public"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 10
         :auto-preamble t)
	("website-static"
         :base-directory "./org"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|html"
         :publishing-directory "./public"
         :recursive t
         :publishing-function org-publish-attachment)))
