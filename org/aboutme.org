#+TITLE: About Me
#+INCLUDE: "config.org"

* Academic Interests
I am interested in quantum field theory, classical mechanics, and numerical modeling. The cross-pollination of traditional "fields" of knowledge is especially important to me. One of my long-running themes is understanding the overlap between quantum mechanics and field theory; the academic presentation of the two topics (to the student) leaves much to be desired in terms of unity. Topics are frequently taught separately in each domain without emphasizing that they are, in fact, the same.

My [[https://rex.libraries.wsu.edu/esploro/outputs/doctoral/Applications-of-Quantum-Interference-in-Bose-Einstein/99901121533901842?institution=01ALLIANCE_WSU][PhD research]] focused on models of Bose-Einstein Condensate (BEC) dynamics and superfluidity. The standard tool is the [[https://en.wikipedia.org/wiki/Gross%E2%80%93Pitaevskii_equation][Gross-Pitaevskii equation (GPE)]], a density functional that can be obtained from first principles in the microscopic theory. I developed semiclassical models of static BEC profiles inspired by the path integral WKB approximation.

* Current Work

For the BEC systems I have worked with in the past, the numerics involved rarely surpassed what was possible on a personal computer. Even the most computationally intensive GPE problems could be scaled to run on a modest in-house GPU, and often there are clever semiclassical methods that can return results in seconds rather than hours.

Fermionic systems rely on a related suite of techniques to the GPE, most notably Density Functional Theory (DFT), in order to model many body systems that cannot be exactly solved. These systems frequently push the boundaries of supercomputing capabilities, and even a modestly sized problem can become computationally inaccessible without proper resources.

In the past I have toyed with properties of the [[https://journals.aps.org/prc/abstract/10.1103/PhysRevC.97.044313][SEALL1]] nuclear DFT in the mean field limit. This treats the system's equation of state as a two-component superfluid, taking the problem from a cutting edge supercomputing problem to a laptop. However, one loses the realistic shell structure found when properly preparing a system with its constituent fermions. Models like this emphasize the fundamental unity of the fermionic/bosonic problem spaces, and I would like to enhance my numerics in order to tackle more sophisticated problems.

[[file:files/corbin_cv.pdf][My CV]]

-----

#+BEGIN_EXPORT HTML
<div class="buttons">
<center>
<a href="https://orcid.org/0009-0008-4725-3271" target="_blank"><img src="./img/orcid.png"></a>
</center>
</div>
#+END_EXPORT

-----

#+INCLUDE: "./footer.org"
