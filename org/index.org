#+TITLE: Ryan's Homepage
#+INCLUDE: "config.org"


* Ryan's Site

Welcome to my simple site. You can read a little about me and my interests, or you can take a peek at some of the pages I have up. Most of the notes begin as demos by myself, to myself, as I learn various topics and techniques. Most of them are recent, with older content being periodically polished and stored here.

* [[file:aboutme.org][About Me]]
* [[file:pages.org][Pages]]

-----

* Recent Updates
#+ATTR_HTML: :style font-size:85%
#+ATTR_HTML: :target _blank
#+INCLUDE: "updates.org" :lines "1-11"
** [[file:updates_page.org][update history]]
-----

#+INCLUDE: "./footer.org"
