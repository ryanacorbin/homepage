#+TITLE: Physics and Numerics
#+INCLUDE: "config.org"

* Technical Notes
*These are all very new works in progress.* I'd like to slowly digitize old work with simple numeric examples. In an effort to get started, I've broken up the topics I commonly work with into spaces for me to put notes as I write them.

** [[https://corbin-physics.gitlab.io/ractools/README.html][Physics, math, and numerics]]
Grouping these separately was hopeless. I needed a place where I could start scripts, notebooks, and toy modules without overthinking what I was doing and gridlocking myself in an organization/optimization problem. The result was this sandbox repo.

The math and numerics pages are notes on techniques and algorithms that were often written in pursuit of a physics topic. The physics notebooks range from dead-simple to things that would be better run outside of a notebook and with some optimization--this includes some sample Schrodinger equation time evolution using spectral methods for the spatial derivatives. Pages written after 2023 are quality controlled, but older ones are not so hot.
** [[https://corbin-physics.gitlab.io/quantum-fields/index.html][Quantum Field Theory]]
This is a notoriously difficult topic. I'm on the hunt for simple examples and problems to keep in my back pocket for future reference. Much of my PhD research involved many-body applications of field theory, as well as path integration, so the topics I'm likely to cover in these notes are heavily biased.

* Old Stuff
I don't quite disown these links, but they could be better.
** [[https://ryanacorbin.gitlab.io/kepler/index.html][Simple 2D Keplerian orbits]]
A sample site I generated while working on simulating classical, Newtonian orbits, and extrapolating the orbital precession of Mercury. The latter was a problem in chapter 4 of [[https://www.physics.purdue.edu/~hisao/book/][Computational Physics]] by Giordano and Nakanishi.
** [[../data/flashcards/flashcards.html][Prelim Study Flashcards]]
My flashcards from long ago, digitized with an even older tool.

-----

#+INCLUDE: "./footer.org"
