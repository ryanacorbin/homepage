# [Homepage](https://ryanacorbin.gitlab.io/homepage/)

This is a simple homepage for sharing things I've worked on.
Most of it will be geared towards physics and computation.

# Quickstart
- Run `make build` to generate this static site from org files.
- Run `make deploy` to build the site, then commit and push the repo.

# Structure

- `build.el` : emacs script for building this static site from org files. This can be run without an emacs instance open through the command line using the `Makefile` command `build`. It will run emacs loading only the script while bypassing the usual `init.el` file, to be sure that it doesn't rely on anything extra hidden in my personal configs.
